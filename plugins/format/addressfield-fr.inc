<?php
/**
 * @file
 * A specific handler for FR.
 */

/**
 * Determine what kind of level to display the field.
 */
define('FR_ADDRESS_CP', 1);
define('FR_ADDRESS_CITY', 2);
define('FR_ADDRESS_DEPARTMENT', 3);
define('FR_ADDRESS_LATITUDE_LONGITUDE', 4);

$plugin = array(
  'title' => t('Address form (FR add-on)'),
  'format callback' => 'addressfield_fr_format_address_fr_generate',
  'type' => 'address',
  'weight' => -80,
);

/**
 * @param $format
 * @param $address
 * @param array $context
 */
function addressfield_fr_format_address_fr_generate(&$format, $address, $context = array()) {
  //@todo define if we will user #required, #default_value

  if ($address['country'] == 'FR' && $context['mode'] == 'form') {
    // Postal code tweaks.
    $format['locality_block']['postal_code']['#title'] = t('Postal code');
    $format['locality_block']['postal_code']['#weight'] = FR_ADDRESS_CP;
    $format['locality_block']['postal_code']['#size'] = '5';
    $format['locality_block']['postal_code']['#maxlength'] ='5';
    $format['locality_block']['postal_code']['#wrapper_id'] = $format['#wrapper_id'];
    $format['locality_block']['postal_code']['#process'][] = 'ajax_process_form';
    $format['locality_block']['postal_code']['#process'][] = 'addressfield_format_address_fr_postal_code_process';
    $format['locality_block']['postal_code']['#element_validate'] = array('addressfield_form_fr_postal_code_validation');
    $format['locality_block']['postal_code']['#ajax'] =
      array(
        'callback' => 'addressfield_standard_widget_refresh',
        'wrapper' => $format['#wrapper_id'],
      );

    if (!empty($address['postal_code'])) {
      $localities = _getCitybyPostalCode_fr($address['postal_code']);

      if (!empty($address['locality'])) {
        $administrative_areas = _getDepartementbyPostal_code_City_fr($address['postal_code'], $address['locality']);
        $latitudelongitude = _getLatitudeLongitudebyPostal_code_City_fr($address['postal_code'], $address['locality']);
      }
      else {
        $administrative_areas = array('' => t('--'));
        $latitudelongitude = array('' => t('--'));
      }
    }else {
      $localities = array('' => t('--'));
      $administrative_areas = array('' => t('--'));
      $latitudelongitude = array('' => t('--'));
    }
    // Locality tweaks.
    $format['locality_block']['locality']['#title'] = t('City');
    $format['locality_block']['locality']['#weight'] = FR_ADDRESS_CITY;
    $format['locality_block']['locality']['#options'] = $localities;
    $format['locality_block']['locality']['#access'] = !empty($address['postal_code']) ? TRUE : FALSE;
    $format['locality_block']['locality']['#process'][] = 'ajax_process_form';
    $format['locality_block']['locality']['#process'][] = 'addressfield_format_address_fr_locality_process';
    $format['locality_block']['locality']['#element_validate'] = array('addressfield_form_fr_locality_validation');
    $format['locality_block']['locality']['#ajax'] =
      array(
        'callback' => 'addressfield_standard_widget_refresh',
        'wrapper' => $format['#wrapper_id'],
      );

    // Administrative_area tweaks.
    $format['locality_block']['administrative_area']['#title'] = t('Department');
    $format['locality_block']['administrative_area']['#weight'] = FR_ADDRESS_DEPARTMENT;
    $format['locality_block']['administrative_area']['#options'] = !empty($administrative_areas) ? $administrative_areas : array('' => t('--'));
    $format['locality_block']['administrative_area']['#process'][] = 'ajax_process_form';
    $format['locality_block']['administrative_area']['#process'][]= 'addressfield_format_address_fr_administrative_area_process';
    $format['locality_block']['administrative_area']['#access'] = !empty($address['locality'/*'postal_code'*/]) ? TRUE : FALSE;

    // Latitude / Longitude tweaks.
    $format['locality_block']['sub_premise']['#prefix'] = '<div style="display:none;">';
    $format['locality_block']['sub_premise']['#title'] = t('Latitude/Longitude');
    $format['locality_block']['sub_premise']['#weight'] = FR_ADDRESS_LATITUDE_LONGITUDE;
    $format['locality_block']['sub_premise']['#options'] = !empty($administrative_areas) ? $latitudelongitude : array('' => t('--'));
    $format['locality_block']['sub_premise']['#access'] = !empty($address['locality']) ? TRUE : FALSE;
    $format['locality_block']['sub_premise']['#suffix'] = '</div>';
  }
  elseif ($address['country'] == 'FR'){
    // Cancel the AJAX for forms we don't control.
    if (isset($format['locality_block']['postal_code'])) {
      $format['locality_block']['postal_code']['#ajax'] = array();
    }
    if (isset($format['locality_block']['locality'])) {
      $format['locality_block']['locality']['#ajax'] = array();
    }
  }
  if ($address['country'] == 'FR' && $context['mode'] == 'render') {
    $format['locality_block']['locality']['#render_type'] = 'addressfield_fr_container';
    $format['locality_block']['locality']['#addressfield_column'] = 'locality';
    $format['locality_block']['administrative_area']['#render_type'] = 'addressfield_fr_container';
    $format['locality_block']['administrative_area']['#addressfield_column'] = 'administrative_area';
    $format['locality_block']['postal_code']['#weight'] = FR_ADDRESS_CP;
    $format['locality_block']['locality']['#weight'] = FR_ADDRESS_CITY;
    $format['locality_block']['administrative_area']['#weight'] = FR_ADDRESS_DEPARTMENT;

    $format['locality_block']['administrative_area']['#access'] = FALSE;
  }
}

/**
 * Form process callback for addressfield postal_code.
 */
function addressfield_format_address_fr_postal_code_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);
  return $element;
}

/**
 * Form process callback for addressfield locality.
 */
function addressfield_format_address_fr_locality_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);
  return $element;
}

/**
 * Form process callback for addressfield locality.
 */
function addressfield_format_address_fr_administrative_area_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);
  return $element;
}
/**
 * Form validation for addressfield postal_code.
 */
function addressfield_form_fr_postal_code_validation($element, &$form_state, &$form) {
  if (empty($element['#value'])) {
    // Nothing to do.
    return;
  }

  // If the postal_code was changed, rebuild the form.
  if ($element['#default_value'] != $element['#value']) {
    $form_state['rebuild'] = TRUE;
  }

  $localities = _getCitybyPostalCode_fr($element['#value']);

  if (!empty($localities)) {
    // Get the base #parents for this address element.
    $adddressfield_parents = array_slice($element['#parents'], 0, -1);
    $first_locality = key($localities);
    $postal_code = drupal_array_get_nested_value($form_state['values'], array_merge($adddressfield_parents, array('postal_code')));

    // Set the new values in the form.
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#default_value')), $first_locality, TRUE);
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#value')), $first_locality, TRUE);
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#options')), $localities, TRUE);

    // Discard value the user has already entered there.
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#default_value')), NULL, TRUE);
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#value')), NULL, TRUE);
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'locality', '#options')), NULL, TRUE);

    $administrative_areas = _getDepartementbyPostal_code_City_fr($postal_code, $first_locality);
    if (!empty($administrative_areas)) {
      $first_administrative_area = key($administrative_areas);

      // Set the new values in the form.
      drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#default_value')), $first_administrative_area, TRUE);
      drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#value')), $first_administrative_area, TRUE);
      drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#options')), $administrative_areas, TRUE);

      // Discard value the user has already entered there.
      drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#default_value')), NULL, TRUE);
      drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#value')), NULL, TRUE);
      drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#options')), NULL, TRUE);

    }
  }
}

/**
 * Form validation for addressfield locality.
 */
function addressfield_form_fr_locality_validation($element, &$form_state, &$form) {
  if (empty($element['#value'])) {
    // Nothing to do.
    return;
  }

  // If the locality was changed, rebuild the form.
  if ($element['#default_value'] != $element['#value']) {
    $form_state['rebuild'] = TRUE;
  }

  // Get the base #parents for this address form.
  $adddressfield_parents = array_slice($element['#parents'], 0, -1);
  $postal_code = drupal_array_get_nested_value($form_state['values'], array_merge($adddressfield_parents, array('postal_code')));
  $administrative_areas = _getDepartementbyPostal_code_City_fr($postal_code, $element['#value']);

  if (!empty($administrative_areas)) {

    $first_administrative_area = key($administrative_areas);

    // Set the new values in the form.
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#default_value')), $first_administrative_area, TRUE);
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#value')), $first_administrative_area, TRUE);
    drupal_array_set_nested_value($form_state['values'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#options')), $administrative_areas, TRUE);

    // Discard value the user has already entered there.
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#default_value')), NULL, TRUE);
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#value')), NULL, TRUE);
    drupal_array_set_nested_value($form_state['input'], array_merge($adddressfield_parents, array('locality_block', 'administrative_area', '#options')), NULL, TRUE);
  }
}

function _getCitybyPostalCode_fr($cp =''){

  if (strlen($cp)  < 5)
    return array('' => t('Short postal code'));

  $liste_destinataires = array('' => t('- City -'));
  $nb_enreg = 0;

  $file = drupal_get_path('module', 'addressfield_fr') . '/includes/city_fr.csv';
  if (!file_exists($file)) {
    return FALSE;
  }
  $handle = fopen($file, "r");

  if ($handle !== FALSE)
  {
    while (($data = fgetcsv($handle)) !== FALSE)
    {
      if ($data[0] == $cp) {
        $liste_destinataires += array($data[2] => $data[1].$data[2]);
        $nb_enreg = $nb_enreg + 1;
      }
    }
    fclose($handle);
  }else
    return FALSE;

  if ($nb_enreg == 0) return array('' => t('Invalid postal code'));
  return $liste_destinataires;
}

function _getDepartementbyPostal_code_City_fr($cp ='', $city=''){

  $administrative_areas_fr = array(
    '01' => 'Ain',
    '02' => 'Aisne',
    '03' => 'Allier',
    '04' => 'Alpes-de-Haute Provence',
    '05' => 'Hautes-Alpes',
    '06' => 'Alpes Maritimes',
    '07' => 'Ardèche',
    '08' => 'Ardennes',
    '09' => 'Ariège',
    '10' => 'Aube',
    '11' => 'Aude',
    '12' => 'Aveyron',
    '13' => 'Bouches-du-Rhône',
    '14' => 'Calvados',
    '15' => 'Cantal',
    '16' => 'Charente',
    '17' => 'Charente-Maritime',
    '18' => 'Cher',
    '19' => 'Corrèze',
    '2A' => 'Corse-du-Sud',
    '2B' => 'Haute-Corse',
    '21' => 'Côte d Or',
    '22' => 'Côtes d Armor',
    '23' => 'Creuse',
    '24' => 'Dordogne',
    '25' => 'Doubs',
    '26' => 'Drôme',
    '27' => 'Eure',
    '28' => 'Eure-et-Loire',
    '29' => 'Finistère',
    '30' => 'Gard',
    '31' => 'Haute-Garonne',
    '32' => 'Gers',
    '33' => 'Gironde',
    '34' => 'Hérault',
    '35' => 'Ille-et-Vilaine',
    '36' => 'Indre',
    '37' => 'Indre-et-Loire',
    '38' => 'Isère',
    '39' => 'Jura',
    '40' => 'Landes',
    '41' => 'Loir-et-Cher',
    '42' => 'Loire',
    '43' => 'Haute-Loire',
    '44' => 'Loire-Atlantique',
    '45' => 'Loiret',
    '46' => 'Lot',
    '47' => 'Lot-et-Garonne',
    '48' => 'Lozère',
    '49' => 'Maine-et-Loire',
    '50' => 'Manche',
    '51' => 'Marne',
    '52' => 'Haute-Marne',
    '53' => 'Mayenne',
    '54' => 'Meurthe-et-Moselle',
    '55' => 'Meuse',
    '56' => 'Morbihan',
    '57' => 'Moselle',
    '58' => 'Nièvre',
    '59' => 'Nord',
    '60' => 'Oise',
    '61' => 'Orne',
    '62' => 'Pas-de-Calais',
    '63' => 'Puy-de-Dôme',
    '64' => 'Pyrenées-Atlantiques',
    '65' => 'Hautes-Pyrenées',
    '66' => 'Pyrenées-Orientales',
    '67' => 'Bas-Rhin',
    '68' => 'Haut-Rhin',
    '69' => 'Rhône',
    '70' => 'Haute-Saône',
    '71' => 'Saône-et-Loire',
    '72' => 'Sarthe',
    '73' => 'Savoie',
    '74' => 'Haute-Savoie',
    '75' => 'Paris',
    '76' => 'Seine-Maritime',
    '77' => 'Seine-et-Marne',
    '78' => 'Yvelines',
    '79' => 'Deux-Sèvres',
    '80' => 'Somme',
    '81' => 'Tarn',
    '82' => 'Tarn-et-Garonne',
    '83' => 'Var',
    '84' => 'Vaucluse',
    '85' => 'Vendée',
    '86' => 'Vienne',
    '87' => 'Haute-Vienne',
    '88' => 'Vosges',
    '89' => 'Yonne',
    '90' => 'Territoire de Belfort',
    '91' => 'Essonne',
    '92' => 'Hauts-de-Seine',
    '93' => 'Seine-Saint-Denis',
    '94' => 'Val-de-Marne',
    '95' => 'Val-d Oise',
    '971' => 'Guadeloupe',
    '973' => 'Guyane',
    '972' => 'Martinique',
    '976' => 'Mayotte',
    '974' => 'La Réunion'
  );
  $liste_destinataires = NULL;
  $nb_enreg = 0;

  $file = drupal_get_path('module', 'addressfield_fr') . '/includes/city_fr.csv';
  if (!file_exists($file)) {
    return FALSE;
  }
  $handle = fopen($file, "r");

  if ($handle !== FALSE)
  {
    while (($data = fgetcsv($handle)) !== FALSE)
    {
      if ($data[0] == $cp && $data[2] == $city) {
        $liste_destinataires = array($data[3] => $administrative_areas_fr[$data[3]].' ['.$data[3].']');
        $nb_enreg = $nb_enreg + 1;
      }
    }
    fclose($handle);
  }else
    return FALSE;
  return $liste_destinataires;
}

function _getLatitudeLongitudebyPostal_code_City_fr($cp ='', $nom=''){

  $liste_destinataires = NULL;
  $nb_enreg = 0;

  $file = drupal_get_path('module', 'addressfield_fr') . '/includes/city_fr.csv';
  if (!file_exists($file)) {
    return FALSE;
  }
  $handle = fopen($file, "r");

  if ($handle !== FALSE)
  {
    while (($data = fgetcsv($handle)) !== FALSE)
    {
      if ($data[0] == $cp && $data[2] == $nom) {
        $liste_destinataires = array(($data[4].', '.$data[5]) => ($data[4].', '.$data[5]));
        $nb_enreg = $nb_enreg + 1;
      }
    }
    fclose($handle);
  }else
    return FALSE;
  return $liste_destinataires;
}